const Course = require("../Models/coursesSchema.js");
const auth = require("../auth.js");
// Create a new course
/*
	Steps:
	1. Create a new Course object using the mongoose model and the information from the request body and the id from the header
	2. Save the new User to the database
*/

module.exports.addCourse = (request, response) => {
    // todo: check headers auth if user is admin
    // todo: decode authorizatioin token
    const userData = auth.decode(request.headers.authorization);

    if (userData.isAdmin) {
        let input = request.body;

        let newCourse = new Course({
            name: input.name,
            description: input.description,
            price: input.price
        });

        // saves the created object to our database
        return newCourse.save().then(course => {
            console.log(course);
            response.send(course);
        })
        .catch(error => {
            console.log(error)
            response.send(false)
        })
    }else{
        response.send(401,"Unauthorized!")
    }

}

// Controller to retrieved all the courses 
module.exports.allCourses = (request, response) => {
    const userData = auth.decode(request.headers.authorization);
    console.log(userData);

    if (!userData.isAdmin) {
        return response.send ("Unauthorized!")
    }else{
        Course.find({})
        .then (result => response.send(result))
        .catch(error => response.send(error))
    }
}

// Controller to retrieved active courses
module.exports.allActiveCourses = (request, response) => {
    Course.find({isActive:true})
    .then(result => response.send(result))
    .catch(error => response.send(error))
}

// Controller to retrieved Inactive courses

module.exports.allInactiveCourses = (request, response) => {
    const userData = auth.decode(request.headers.authorization);
    console.log(userData);

    if (!userData.isAdmin) {
        return response.send ("Unauthorized!")
    }else{
        Course.find({isActive:false})
        .then(result => response.send(result))
        .catch(error => response.send(error))
    }
}

// Controller to get specific course
module.exports.courseDetails = (request, response) => {
const courseId = request.params.courseId

    Course.findById(courseId)
    .then(result => response.send(result))
    .catch(error => response.send(error));
}

// controller to update specific course
// Logic:
    // 1. we are going to edit/update the course that is stored in the params

module.exports.updateCourse = (request, response) =>{
    const userData = auth.decode(request.headers.authorization);

    const courseId = request.params.courseId

    const input = request.body

    if(!userData.isAdmin){
        return response.send ("Unauthorized!")
    }else{
       Course.findOne({_id: courseId})
       .then(result =>{
        if (result === null){
            return response.send("Invalid CourseId! Please try again")
        }else{
            let updatedCourse = {
                name: input.name,
                description: input.description,
                price: input.price
            }
            Course.findByIdAndUpdate (courseId, updatedCourse, {new:true})
            .then(result => {
            console.log(result)
            response.send(result)})
            .catch(error => response.send(error));
        }
       })
    }
}

// Controller to archive course
module.exports.archiveCourse = (request, response) =>{
    const userData = auth.decode(request.headers.authorization);
    const courseId = request.params.courseId
    const input = request.body
    
    if(!userData.isAdmin){
        return response.send ("Unauthorized!")
    }else{
       Course.findOne({_id: courseId})
       .then(result =>{
        if (result === null){
            return response.send("Invalid CourseId! Please try again")
        }else{
            let updatedCourse = {
                isActive: input.isActive
            }
            Course.findByIdAndUpdate (courseId, updatedCourse, {new:true})
            .then(result => {
            console.log(result)
            response.send(true)})
            .catch(error => response.send(error));
        }
       })
    }
}
    



