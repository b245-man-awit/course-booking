const mongoose = require("mongoose");
const User = require("../Models/usersSchema.js");
const Course = require("../Models/coursesSchema.js");
const bcrypt = require("bcrypt");
const auth = require("../auth.js");


// Controllers

// This controller will create or register a user on our database
module.exports.userRegistration = (request, response) => {
    const input = request.body;

    User.findOne({ email: input.email }).then(result => {
        if (result !== null) {
            return response.send("The email is already taken!")
        } else {
            let newUser = new User({
                firstName: input.firstName,
                lastName: input.lastName,
                email: input.email,
                password: bcrypt.hashSync(input.password, 10),
                mobileNo: input.mobileNo
            })
            newUser.save().then(save => {
                return response.send("You are now registered to our website!")
            }).catch(error => {
                return response.send(error)
            })
        }
    })
        .catch(error => {
            return response.send
        })

}

// user Authentication
module.exports.userAuthentication = (request, response) => {
    let input = request.body;

    // Possible scenarios when logging in
    // 1. email is not yet registered
    // 2. email is registered but the password is wrong

    User.findOne({ email: input.email }).then(result => {
        if (result === null) {
            return response.send("Email is not yet registered. Register first before logging in!")
        } else {
            // verify if pw is correct
            // the "compaseSync" method is used to compare a non encrypted password to the encrypted password
            // retrurn boolean value, if match true will return otherwise false
            const isPasswordCorrect = bcrypt.compareSync(input.password, result.password)

            if (isPasswordCorrect) {
                return response.send({ auth: auth.createAccessToken(result) })
            } else {
                return response.send("Password is incorrect!")
            }
        }
    })
        .catch(error => {
            return response.send(error);
        })
}

// Activity
// controller for getting user profile
module.exports.userProfile = (request, response) => {
    // let id = request.body.id;
    const userData = auth.decode(request.headers.authorization);

    console.log(userData)
    User.findById(userData._id).then(result => {
        return response.send({
            ...result._doc,
            password: ''
        })
    })
        .catch(error => {
            return response.send(error)
        })
}

// Controller for user enrollment
// get the id of the user by decoding the jwt
// get the courseId by using the request params
module.exports.enrollCourse = async (request, response) => {
    // First we have to get the userId and the courseId

    // decode the token to extract/unpack the payload
    const userData = auth.decode(request.headers.authorization);

    // get courseId using params in the url
    const courseId = request.params.courseId;

    // to do:
    //1. push the courseId in the enrollment property of the user
    //2. push the user id in the enrollees property of the course
    if (userData.isAdmin) {
        return response.send("Admin is not allowed to enroll!")
    }

    const user = await User.findById(userData._id).exec()
    if (!user) {
        return response.send('Enrollment failed: Invalid User')
    }

    const course = await Course.findById(courseId).exec().catch(() => (null))
    if (!course) {
        return response.send("Enrollment failed: Course is invalid!")
    }

    try {
        user.enrollments.push({ courseId: course._id })
        await user.save()

        course.enrollees.push({ userId: user._id })
        await course.save()

        return response.send("The course is now enrolled!")
    } catch (error) {
        return response.send(500, "There was an error during the enrollment. Please try again!")
    }
};


