/*
bcrypt (encrypt pw)
    - installing: npm i bcrypt
    - In our application we will be using this package to demonstrate how encrypt data when a user register to our website
    - This package is one of many packages that we can use to encrypt information but it is not commonly recommended because of how simple the algo is.
    - There are other more advanced encryption packages that can be use
    -Syntax for hashsiong password:
        bcrypt.hashSync(password, saltRounds)
            where:
                saltRounds is the value provided as the number of "salt" round that the bcrypt algorith will run in order to encrypt the password.

JWT - jsonwebtoken package (encrypt data)
    - JSON web tokens is an industry standard for sending info between our applications in a secure manner
    - this package will allow us to gain access to methods that will help us create JSON web token


Activity Other SOlution:
    module.exports.userProfile = (request, response) => {
        let input = request.body;

        return User.findById(input.id).then(result => {
            result.password = "";

            return response.send.(result);
        })
    }



*/