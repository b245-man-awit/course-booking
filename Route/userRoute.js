const express = require("express");
const router = express.Router();
const auth = require("../auth.js");

const userController = require("../Controllers/userController.js")

// [Routes]
// this is responsible for the registration of the user
router.post("/register", userController.userRegistration);

// Route for user authentication
router.post("/login", userController.userAuthentication);

// Activity
// Route to retrieve user details:
router.get("/details", auth.verify, userController.userProfile);

// Route for user enrollment
router.post("/enroll/:courseId", auth.verify, userController.enrollCourse);


module.exports = router;