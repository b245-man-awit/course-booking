const express = require("express");
const router = express.Router();
const auth = require("../auth.js");

const courseController = require("../Controllers/courseController.js");

// ROUTES WITHOUT PARAMS
// Route for creating a course
router.post("/", courseController.addCourse);

// Route for retrieving all courses
router.get("/all", auth.verify, courseController.allCourses)

// router to get all active courses
router.get("/allActive", courseController.allActiveCourses)

// router to get all inactive courses
router.get("/allInactive", auth.verify, courseController.allInactiveCourses)


// ROUTES WITH PARAMS 
    // kasi nageeror pag naglagay ng route n walang params after ng route na may params
    // so need lagi ilagay sa dulo lahat ng routes na may params
// Route to retrieve specific course details
router.get("/:courseId", courseController.courseDetails)

// Router to update courses
router.put("/update/:courseId", auth.verify, courseController.updateCourse);

// Router to archive courses
router.patch("/archive/:courseId", auth.verify, courseController.archiveCourse)




module.exports = router;
