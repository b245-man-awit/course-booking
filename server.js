const express = require("express");
const mongoose = require("mongoose");
// by default our backend 's CORS setting will prevent any application outside our expressJS app to process the request. using the cors package, it will allow us to manipulate this and control what application may use or app

// allows our backend application to be available to our frontend application
// allows us to control the app's  Cross origin Resource Sharing
const cors = require("cors");

const usersRoute = require("./Route/userRoute.js");
const courseRoute = require("./Route/courseRoute.js");


const port = 3001;
const app = express();
// para matanggal ung strict query nalumalabas sa terminal pag ni rarun
mongoose.set('strictQuery',true);
// MongoDB COnnection
mongoose.connect("mongodb+srv://admin:admin@batch245-man-awit.8gpp4b2.mongodb.net/batch245_Course_API_ManAwit?retryWrites=true&w=majority", {
        useNewUrlParser: true,
        useUnifiedTopology: true
    })

    let db = mongoose.connection;

     // error handling
     db.on("error", console.error.bind(console, "Connection error!"));

     // Validation of the connection
     db.once("open", ()=> console.log("We are connected to the cloud!"))
 

// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended: true})); 
app.use(cors());

app.use("/users", usersRoute);
app.use("/course", courseRoute);







app.listen(port, ()=> console.log(`Server is running at port ${port}!`))

